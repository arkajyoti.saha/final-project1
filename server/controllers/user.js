const User = require('../models/users');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const Candidate = require('../models/candidates');
const validatePhoneNumber = require('validate-phone-number-node-js');
async function hashPassword(password) {
 return await bcrypt.hash(password, 10);
}
 
async function validatePassword(plainPassword, hashedPassword) {
 return await bcrypt.compare(plainPassword, hashedPassword);
}
exports.middleware = async(req, res, next) =>{
    try{
        const accessToken = req.headers["x-access-token"];
        const { userId, exp } = await jwt.verify(accessToken, process.env.JWT_SECRET);
        // Check if token has expired
        if (exp < Date.now().valueOf() / 1000) { 
            return res.status(401).json({ error: "JWT token has expired" });
        }
        req.userId = userId;
        next();
    }
    catch(err){
        console.log(err);
    }
}
exports.create = async (req, res, next) => {
 try {
  const { name, phone, active, type, password} = req.body;
  var pass = req.body.password.toString();
  var no = req.body.phone.toString();
//   console.log(no.length);
  if(pass.length<7){
    return res.status(401).json({message: "entered incorrect credentials"});
  }
  const result = validatePhoneNumber.validate(no);
  if(!result){
    return res.status(401).json({message: "entered incorrect phone number type"});
  }
  const hashedPassword = await hashPassword(password);
  const newUser = new User({ name, phone, active: active, password: hashedPassword, type: type || "ta_exec"});
  await newUser.save();
  res.json({
   data: newUser
  })
 } catch (error) {
  next(error)
 }
};

exports.login = async (req, res, next) => {
try {
    const { name, password } = req.body;
    const user = await User.findOne({ name });
    if (!user) return next(new Error('Ta dont exist'));
    const validPassword = await validatePassword(password, user.password);
    if (!validPassword) return res.status(401).json({message: "you entered wrong credentials"});
    const accessToken = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, {
    expiresIn: "1d"
    });
    await User.findByIdAndUpdate(user._id, { accessToken });
    user.accessToken = accessToken;
    user.active = true;
    user.updated_at = Date.now();
    // console.log(Date.now());
    user.save();
    res.status(200).json({
    user,
    accessToken,
    message: 'you are logged in'
    })
    } catch (error) {
        next(error);
    }
};

exports.onboard = async(req, res, next)=> {
    try{
        const userId = req.userId;
        // console.log(userId);
        const Ta_Exec = await User.findById(userId);
        if(Ta_Exec.type.toString() != 'ta_exec'){
            return res.status(401).json({
                message: "you are not ta so not allowed to create interviewer model bcs u r not TaExec"
            });
        }
        // console.log(Ta_Exec.active);
        if(Ta_Exec.active === false){
            return res.status(401).json({
                message: "you are not logged in"
            });
        }
        const { name, phone, active,type, password} = req.body; 
        const hashedPassword = await hashPassword(password);
        const newUser = new User({ name, phone, active: active,password: hashedPassword, type: type || "interviewer"});
        await newUser.save();
        res.json({
            data: newUser
        })
    }
    catch(err){
        console.log(err);
    }
}
exports.logout = async (req, res, next) => {
    try {
        const { name, password } = req.body;
        const user = await User.findOne({ name });
        const validPassword = await validatePassword(password, user.password);
        if (!validPassword) return res.status(401).json({message: "you entered wrong credentials"});
        const accessToken = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, {
        expiresIn: "1d"
        });
        user.active = false;
        await User.findByIdAndUpdate(user._id, { accessToken })
        res.status(200).json({
        // data: { userId: user._id, email: user.email, role: user.role },
        user,
        message: 'you are logged out'
        })
        } catch (error) {
            next(error);
        }
    };

exports.getUsers = async (req, res, next) => {
    const userId = req.userId;
    const Ta_Exec = await User.findById(userId);
    if(Ta_Exec.type.toString() != 'ta_exec'){
        return res.status(401).json({
            message: "you are not TA so not allowed to get the candidates details"
        });
    } 
    const users = await Candidate.find({});
    res.status(200).json({
     data: users
    });
}
    
exports.getUser = async (req, res, next) => {
try {
    const userId = req.params.userId;
    console.log(userId);
    const user = await User.findById(userId._id);
    if (!user) return next(new Error('User does not exist'));
    res.status(200).json({
    data: user
    });
} catch (error) {
    next(error)
    }
}
    
exports.updateUser = async (req, res, next) => {
try {
    const update = req.body
    const userId = req.params.userId;
    await User.findByIdAndUpdate(userId._id, update);
//  const user = await User.findById(userId)
    res.status(200).json({
    data: user,
    message: 'User has been updated'
    });
} catch (error) {
    next(error)
    }
}
    
exports.deleteUser = async (req, res, next) => {
try {
    const userId = req.params.userId;
    await User.findByIdAndDelete(userId);
    res.status(200).json({
    data: null,
    message: 'User has been deleted'
    });
} catch (error) {
    next(error)
    }
}

const { roles } = require('../roles');
const { aggregate } = require('../models/users');
const { promise } = require('bcrypt/promises');
const req = require('express/lib/request');
 
exports.grantAccess = function(action, resource) {
return async (req, res, next) => {
    try {
    const permission = roles.can(req.user.type)[action](resource);
    if (!permission.granted) {
    return res.status(401).json({
        error: "You don't have enough permission to perform this action"
    });
    }
    next()
    } catch (error) {
        next(error)
        }
    }
}
    
exports.allowIfLoggedin = async (req, res, next) => {
try {
    const user = res.locals.loggedInUser;
    if (!user)
    return res.status(401).json({
    error: "You need to be logged in to access this route"
    });
    req.user = user;
    next();
    } 
    catch (error) {
    next(error);
    }
}
