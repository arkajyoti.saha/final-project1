const User = require('../models/candidates');
const Interview = require('../models/interview');
const jwt = require('jsonwebtoken');
const ta = require('../models/users');

exports.middleware = async(req, res, next) =>{
    try{
        const accessToken = req.headers["x-access-token"];
        const { userId, exp } = await jwt.verify(accessToken, process.env.JWT_SECRET);
        // Check if token has expired
        if (exp < Date.now().valueOf() / 1000) { 
            return res.status(401).json({ error: "JWT token has expired, login to obtain a new one" });
        }
        req.userId = userId;
        next();
    }
    catch(err){
        console.log(err);
    }
}

exports.create = async (req, res, next) => {
    try {
    const userId = req.userId; 
    const Ta_Exec = await ta.findById(userId);
    if(Ta_Exec.type.toString() != 'ta_exec'){
        return res.status(401).json({
            message: "you are not ta so not allowed to create candidate model"
        });
    }

    const { name, phone, status, resume, slot, interviewer} = req.body;
    const newUser = new User({ name, phone, status : status || "created", resume, slot, interviewer });
     await newUser.save();
     res.json({
        message: 'candidate model has been created',
        data: newUser
     })
    } catch (error) {
     next(error)
    }
   };

exports.updateSlot = async (req, res, next) => {
    try {
        const userId = req.userId;
        const Ta_Exec = await ta.findById(userId);
        if(Ta_Exec.type.toString() != 'ta_exec'){
            return res.status(401).json({
                message: "you are not ta so not allowed to update candidate model bcs u r not TA"
            });
        }
        const update = req.body
        // console.log(update.slots);
        // console.log(update.resume);
        const updateSlot = update.slots;
        for(var i=0;i<update.slots.length;i++){
            // console.log(update.slots[i].start);
            if(update.slots[i].end - update.slots[i].start != 3600000){
                return res.status(401).json({
                    message: "not scheduled an hour interview"
                });
            }
        }
        const userIdparam = req.params.userId;
        // const user = await User.findOneAndUpdate({userIdparam}, {slots: update["slots"]}, {new: true});
        const user = await User.findById(userIdparam);
        const n = update.slots.length;
        for(let i=0;i<n;i++){
            user.slots.push(update.slots[i]);
        }
        // const user = await User.findById(userId);
        // user.resume = update.resume;
        user.save();
        res.status(200).json({
         data: user,
         message: `Candiate model has been updated`
        });
       } catch (error) {
        next(error)
    }
}

exports.getUser = async (req, res, next) => {
    try {
        const userId = req.params.userId;
        const user = await User.findById(userId);
        // console.log(user);
        const interview_id = user.interview._id;
        // console.log(interview_id._id);
        let interview;
        Interview.find({_id: interview_id}, (err, interviewO1) =>{
            interview = interviewO1[0];
            // console.log(interviewO1);
            if (!user) return next(new Error('User does not exist'));
            res.status(200).json({
            data: user,
            interview: interviewO1[0]
            });
        })
        
    } catch (error) {
        next(error)
    }
}

const { roles } = require('../roles');
const candidates = require('../models/candidates');
 
exports.grantAccess = function(action, resource) {
return async (req, res, next) => {
    try {
    const permission = roles.can(req.user.type)[action](resource);
    if (!permission.granted) {
    return res.status(401).json({
        error: "You don't have enough permission to perform this action"
    });
    }
    next()
    } catch (error) {
        next(error)
        }
    }
}

exports.allowIfLoggedin = async (req, res, next) => {
    try {
        const user = res.locals.loggedInUser;
        if (!user)
        return res.status(401).json({
        error: "You need to be logged in to access this route"
        });
        req.user = user;
        next();
        } 
        catch (error) {
        next(error);
        }
    }