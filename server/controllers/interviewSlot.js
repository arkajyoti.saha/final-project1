const Candidate = require('../models/candidates');
const jwt = require('jsonwebtoken');
const User = require('../models/users');
const Interview_slot = require('../models/interviewSlot');

exports.middleware = async(req, res, next) =>{
    try{
        const accessToken = req.headers["x-access-token"];
        const { userId, exp } = await jwt.verify(accessToken, process.env.JWT_SECRET);
        // Check if token has expired
        if (exp < Date.now().valueOf() / 1000) { 
            return res.status(401).json({ error: "JWT token has expired" });
        }
        req.userId = userId;
        next();
    }
    catch(err){
        console.log(err);
    }
}

exports.create = async (req, res, next) => {
    try{
        const userId = req.userId; 
        const interviewer = await User.findById(userId);
        if(interviewer.type.toString() != 'interviewer'){
            return res.status(401).json({
                message: "you are not ta so not allowed to create interviewSlot model bcs u r not interviewer"
            });
        }
        if(interviewer.active === false){
            return res.status(401).json({
                message: "you are not logged in"
            });
        }
        const {slots} = req.body; 
        const newUser = new Interview_slot({ slots: slots});
        newUser.interviwer_id = interviewer;
        await newUser.save();
        res.json({
            data: newUser
        })
    }
    catch(err){
        console.log(err);
    }
    
};


exports.updateSlot = async (req, res, next) => {
try {
    const userId = req.userId;
    const interviewer = await User.findById(userId);
    if(interviewer.type.toString() != 'interviewer'){
        return res.status(401).json({
            message: "you are not interviewer so not allowed to update Interview Slot"
        });
    }

    const update = req.body;
    for(var i=0;i<update.slots.length;i++){
        // console.log(update.slots[i].start);
        if(update.slots[i].end - update.slots[i].start != 3600000){
            return res.status(401).json({
                message: "not scheduled an hour interview"
            });
        }
    }
    const userIdparam = req.params.userId;
    // console.log(userIdparam);
    // console.log(update)
    const user = await Interview_slot.findById(userIdparam);
    // console.log(update.slots);
    const n = update.slots.length;
    for(let i=0;i<n;i++){
        user.slots.push(update.slots[i]);
    }
    // console.log(user.slots);
    user.save();
    // console.log(update.slots);
    // const user = await Interview_slot.findOneAndUpdate({userIdparam}, {slots: update["slots"]}, {new: true});
    res.status(200).json({
        data: user,
        message: 'interviewr_slot has been updated'
    });
    } catch (error) {
    next(error)
    }
}


exports.deleteSlot = async (req, res, next) => {
    try {
    const userId = req.userId; 
    const interviewer = await ta.findById(userId);
    if(interviewer.type.toString() != 'interviewer'){
        return res.status(401).json({
            message: "you are not interviewer so not allowed to delete Interview Slot"
        });
    }
    userId = req.params.userId;
    await User.findByIdAndDelete(userId);
    res.status(200).json({
    data: null,
    message: 'interviewr_slot has been deleted'
    });
    } catch (error) {
     next(error)
    }
}
