const mongoose = require('mongoose');
console.log("inside models/interview");

const interview = new mongoose.Schema({
    // _id: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     required:true,
    //     unique:true
    // },
    candidate_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'candidate',
        // required: true,
        trim: true
    },
    interviewer_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        // required: true,
        trim: true
    },
    // interview_slot: {
    //     timestamps: {
    //         start: 'created_at', // Use `created_at` to store the created date
    //         end: 'updated_at' // and `updated_at` to store the last updated date
    //     },
    //     required: true
    // },
    rating: {
        type: String,
        enum: ['1', '2', '3', '4', '5']
    },
    feedback: {
        type: String,
        // required:true,
        trim:true
    },
    status: {
        type: String,
        enum: ['created','selected', 'rejected']
    }
});

module.exports = mongoose.model('interview',interview);