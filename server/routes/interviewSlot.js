const express = require('express');
const router = express.Router();
const userController = require('../controllers/interviewSlot');
 
router.post('/createInterviewSlot',userController.middleware,userController.create);
router.put('/updateSlot/:userId',userController.middleware ,userController.updateSlot);
router.delete('/deleteSlot/:userId',userController.middleware ,userController.deleteSlot);

module.exports = router;