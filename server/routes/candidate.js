const express = require('express');
const router = express.Router();
const userController = require('../controllers/candidate');
console.log("i am inside routes/candidate");
 
router.post('/createCandidate',userController.middleware ,userController.create);
router.put('/candiateSlot/:userId',userController.middleware,userController.updateSlot);
router.get('/candidate/:userId', userController.getUser);

module.exports = router;

